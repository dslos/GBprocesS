Mapping and further processing
==============================

We recommend using `BWA-MEM <http://bio-bwa.sourceforge.net/bwa.shtml>`_ to map short GBS reads to a reference sequence and to analyze 
the read depth distribution with SMAP delineate (available soon).


----

Single-digest GBS and single-end sequencing
-------------------------------------------

.. image:: images/separately_SE_SD-GBS_SMAP_legend_2.png

----

Single-digest GBS and paired-end sequencing, mapped separately
-----------------------------------------------------------------

.. image:: images/separately_PE_SD-GBS_SMAP_legend_2.png

----

Single-digest GBS and paired-end sequencing, merged
-----------------------------------------------------------------

.. image:: images/merged_PE_SD-GBS_SMAP_legend_2.png

----

Double-digest GBS and single-end sequencing
-------------------------------------------

.. image:: images/separately_SE_DD-GBS_SMAP_legend_2.png

----

Double-digest GBS and paired-end sequencing, mapped separately
--------------------------------------------------------------------

.. image:: images/separately_PE_DD-GBS_SMAP_legend_2.png

----

Double-digest GBS and paired-end sequencing, merged
--------------------------------------------------------------------

.. image:: images/merged_PE_DD-GBS_SMAP_legend_2.png